import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page.HomePage;

import java.util.concurrent.TimeUnit;

public class ShopFlow {
    HomePage homePage;
    WebDriver driver;

    @Given("the user access to the main page")
    public void mainPage2 () {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://automationpractice.com/index.php");}

        @And("click on the Sign in Button")
        public void signIn2(){
        homePage = new HomePage(driver);
        homePage.signInButton();}

        @And("fill the Email address from Already registered")
        public void fillUserEmail1(){homePage.fillEmail();}

        @And("fill the Password \"Abc123..\"")
        public void fillUserPassword2(){homePage.userPassword();}

        @And("click on the sign in button from Already registered")
         public void clickSignIn1(){homePage.signInButtonFromAlreadyRegister();}

        @And("The User will be successfully logged in")
        public void userLoggedIn1(){homePage.checkAccountCreation();}

        @And("click on T-short button")
        public void clickOnTShortButton(){homePage.selectTShort(); }

        @And("click on Add to cart button for t-short")
        public void clickOnAddToCartButton(){homePage.addTocCartTShort();}

        @And("click Continue shoping")
        public void clickOnContinueButton1(){}

         @And("click Woman button")
         public void clickOnWomanButton(){}

        @And("click Casual dress")
        public void clickOnCasualDress(){}

    @And("click on Add to cart button for dress")
    public void clickOnAddToCartDress(){}

    @And("click on proceeded to checkout from summary")
    public void clickOnCheckOutSummary(){}

    @And("click on Proceeded to checkout from address")
    public void clickOnCheckOutAddress(){}

    @And("click on I agree with terms")
    public void clickOnIAgreeTerms(){}

    @And("click on Proceeded to checkout shipping")
    public void clickOnCheckOutShipping(){}

    @And("click on Pay by bank wire")
    public void clickOnPayBank(){}

    @When("click on i confirm my order")
    public void clickOnConfirmOrder(){}

    @Then("the order is confirmed")
    public void pageConfirmed(){}

}
