import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page.HomePage;

import java.util.concurrent.TimeUnit;

public class ProcessOfSignIn {
    HomePage homePage;
    WebDriver driver;

    @Given("the user accesses to the main Page")
    public void mainPage () {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://automationpractice.com/index.php");}

        @And("click on the sign in Button")
             public void SignIn1(){
            homePage = new HomePage(driver);
            homePage.signInButton();
        }
        @And("fill the email address from Already registered")
         public void fillUserEmail(){homePage.fillEmail();}

         @And("fill the password \"Abc123..\"")
        public void fillUserPassword(){homePage.userPassword();}

        @When("click on the Sign in button from Already registered")
        public void clickSignIn(){homePage.signInButtonFromAlreadyRegister();}

        @Then("The user will be successfully logged in")
        public void userLoggedIn(){homePage.checkAccountCreation();}




}
