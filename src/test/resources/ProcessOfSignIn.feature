Feature: A customer want to sign in

  Scenario: A customer want to sign in - positive scenario
    Given the user accesses to the main Page
    And click on the sign in Button
    And fill the email address from Already registered
    And fill the password "Abc123.."
    When click on the Sign in button from Already registered
    Then The user will be successfully logged in

