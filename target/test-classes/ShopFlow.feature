Feature: A customer want to buy 2 products

  Scenario: A customer want to buy a Faded Short Sleeve T-shirts and a casual dress - positive scenario
    Given the user access to the main page
    And click on the Sign in Button
    And fill the Email address from Already registered
    And fill the Password "Abc123.."
    And click on the sign in button from Already registered
    And The User will be successfully logged in
    And click on T-short button
    And click on Add to cart button for t-short
    And click Continue shoping
    And click Woman button
    And click Casual dress
    And click on Add to cart button for dress
    And click on proceeded to checkout from summary
    And click on Proceeded to checkout from address
    And click on I agree with terms
    And click on Proceeded to checkout shipping
    And click on Pay by bank wire
    When click on i confirm my order
    Then the order is confirmed



